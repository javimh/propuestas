---
layout: 2020/post
section: propuestas
category: workshops
title: Mosaicos are back
---

Los gestores de ventanas en mosaico no llegaron a triunfar sobre los de ventanas flotantes. Sin embargo en los últimos años han ganado popularidad. Su minimalismo, capacidad de personalizacion pero sobre todo lo productivos que son los convierten en el arma perfecta del *power user*. A diferencia de los clásicos gestores de ventanas flotante donde al abrir una ventana esta aparece en cualquier lado tapando a otras ventanas, los gestores en mosaico automaticamente organizan las ventanas para ocupar todo el espacio disponible sin quitarte la visión sobre otras aplicaciones. Además permiten configurar todo mediante atajos de teclado, con un movimiento de dedos podremos abrir un programa, moverla a otro espacio de trabajo y cerrarlo sin necesidad de recurrir al ratón o al touchpad de nuestro portátil. 

## Objetivos a cubrir en el taller

Conocer los gestores de ventanas en mosaico y configurar algunos de ellos. Además veremos herramientas y aplicaciones interesantes para nuestro escritorio en mosaico.

## Público objetivo

Usuarios de GNU/Linux tan quemados del touchpad como yo.

## Ponente(s)
Javier Montes, programador y usuario de GNU/Linux. He presentado la charla "Educación en el Software Libre" en JASYP 2018.

### Contacto(s)

* Javier Montes Heras: @Yorozuya3

## Prerrequisitos para los asistentes

Estar minimamente familiarizado con GNU/Linux.

## Prerrequisitos para la organización

Es necesario que cada asistende disponga de su propio ordenador con una distribución  GNU/Linux instalada. No es necesario el ratón ;). 

## Tiempo

Aproximadamente 1 hora.

## Día

Es indiferente.

## Comentarios

Ninguno.

## Condiciones

* [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/).
* [x] &nbsp;Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
* [x] &nbsp;Acepto coordinarme con la organización de esLibre.
