---
layout: 2020/post
section: propuestas
category: workshops
title: Taller de observabilidad en tu malla de servicios con with Istio, Kiali y MiniKube
state: cancelled
---

Este taller consistirá en tratar con la observabilidad en mallas de servicios con Istio, Kiali y MiniKube.

## Objetivos a cubrir en el taller

Los asistentes montaran un entorno de microservicios con MiniKube y lo securizaran con Istio, se ejecutaran las demos de [Istio.io](https://istio.io/) como la aplicación Bookinfo y se trabajara con el entorno y se visualizara con Kiali.

## Público objetivo

Estudiantes de FP, universitari@s, profesionales o cualquier usuari@ interesad@ en la seguridad en microservicios.

## Ponente(s)

**Alberto Jesus Gutierrez Juanes**. Ingeniero Senior en RedHat trabajando en ManageIQ, Istio y Kiali con Ror, Go, Angular y React. Anteriormente trabajando en BBVA como Arquitecto de sistemas en Hadoop y desarrollador. Alberto trabaja ahora desarrollando y manteniendo proyectos de código libre. Este taller se ha dado en la Commit Conf y Codemotion.

### Contacto(s)

**Alberto Jesus Gutierrez Juanes**: aljesusg at gmail dot com

## Prerrequisitos para los asistentes

-   Ordenador:
    -   <https://kubernetes.io>
    -   <https://istio.io/>
-   Conocimientos mínimos: saber qué es un contenedor, pod, servicio, etc; así como conocimientos mínimos de Kubernetes.

## Prerrequisitos para la organización

Ninguno.

## Tiempo

Dos horas, ya que pueden surgir dificultades dependiendo del portátil del asistente.

## Día

Indiferente.

## Comentarios

Ninguno.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x] &nbsp;Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
