---
layout: 2020/post
section: propuestas
category: workshops
title: Introducción a Pharo&#58 programando en un entorno de objetos vivos
state: confirmed
---

Taller de introducción al entorno de programación Pharo Smalltalk que nos ayudará a resolver la siguientes preguntas.

-   ¿Por qué debería interesarme Pharo, o incluso dedicar algo de tiempo a aprenderlo?
-   Exactamente, ¿qué lo hace un entorno tan especial?
-   ¿Qué ventajas tiene programar en un entorno de objetos vivos?
-   Un momento, pero ¿no está Pharo basado en Smalltalk, un lenguaje viejuno y obsoleto?

## Objetivos a cubrir en el taller

Esta charla tiene por objetivo introducir el entorno de programación Pharo Smalltalk, su comunidad y principios subyacentes.

La charla tendrá un carácter eminentemente práctico y se tratará de transmitir, a través de ejemplos concretos, en qué consiste la experiencia de programar inmerso en un entorno de objetos vivos.

Además, se describirán casos de éxito en los que se emplea actualmente Pharo en la industria. Incluida nuestra experiencia propia en distintos proyectos en OSOCO.

##### ¿Por qué debería interesarme Pharo, o incluso dedicar algo de tiempo a aprenderlo?

Porque Pharo es el nuevo lenguaje "cool" en el campo de los lenguajes orientados a objetos.

##### Exactamente, ¿qué lo hace un entorno tan especial?

Pharo es un entorno de programación potente y elegante. Sin constructores, ni declaraciones de tipos, ni tipos primitivos. Es un lenguaje orientado a objetos puro, dinámico y sencillo, cuya sintaxis se puede escribir en una servilleta. Pharo consiste única y completamente en objetos y paso de mensajes, pero sobre todo en un entorno de programación vivo que coloca a los objetos en el centro.

##### ¿Qué ventajas tiene programar en un entorno de objetos vivos?

Una característica única de Pharo es que mientras se programa, el desarrollador se encuentra inmerso en un mundo de objetos vivos, en lugar de archivos de texto ~~muertos~~ estáticos. Esto hace posible obtener un feedback inmediato de los objetos de tu aplicación, lo que convierte la programación en una experiencia única, mucho más productiva y divertida que en los lenguajes basados en texto estático a los que estás acostumbrado.

Además, Pharo es metacircular, es decir, está escrito en sí mismo y consiste en un mundo de objetos directamente accesibles por el programador. Imagina un entorno en el que puedes modificar el compilador, personalizar las herramientas de desarrollo o incluso crear un entorno de desarrollo específico para cada dominio.

##### Un momento, pero ¿no está Pharo basado en Smalltalk, un lenguaje viejuno y obsoleto?

Pharo está inspirado en Smalltalk. Concretamente, es un fork de Squeak, un entorno Smalltalk open source creado por el equipo original de Smalltalk 80 (Dan Ingalls y Alan Kay).

Smalltalk siempre tuvo el inconveniente de adelantarse demasiado a su tiempo. Por ejemplo, fue un lenguaje de programación orientado a objetos antes de que la orientación a objetos se pusiera de moda, de hecho, antes de que el concepto orientado a objetos ni siquiera se hubiera acuñado. También fue un lenguaje dinámico antes de que los lenguajes dinámicos estuvieran de moda.

Actualmente, la mayoría de los impedimentos técnicos para que Smalltalk triunfara se han superado y, por otro lado, Smalltalk todavía está en muchos aspectos por delante de sus sucesores, especialmente en su visión de un entorno en el que todo es un objeto y cualquier cosa puede modificarse en tiempo de ejecución.

Pharo está inspirado en Smalltalk, pero es un Smalltalk moderno y open source.

## Público objetivo

Cualquier persona con conocimientos generales de programación.

## Ponente(s)

-   **Rafael Luque Leiva**

### Contacto(s)

-   **Rafael Luque**: rafael.luque at osoco dot es

Desarrollador de software en [OSOCO](https://osoco.es) y fundador de [Blue Plane](https://blueplane.xyz), un grupo de investigación sin ánimo de lucro, cuya misión es incubar proyectos de I+D con la intención de conseguir un cambio de paradigma que redefina nuestra forma de pensar y abordar el desarrollo de software. Contribuidor de la comunidad Pharo Smalltalk y organizador del Madrid Smalltalk User Group.

## Prerrequisitos para los asistentes

No serán necesarios conocimientos avanzados de programación, ni conocimientos previos de Pharo.

## Prerrequisitos para la organización

-   Proyector o monitor para mostrar slides y código a los participantes.
-   Conexión a Internet.

## Tiempo

Aproximadamente 1 hora.

## Día

Indiferente.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [x] &nbsp;Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
