---
layout: 2020/post
section: propuestas
category: talks
title: CryptPad&#58 open source technology at the service of privacy
state: confirmed
---

The objective is to present new open source technology using end to end encryption while providing very advanced collaboration features.

## Proposal format

-   [x] &nbsp;Talk (25 minutes)
-   [ ] &nbsp;Lightning talk (10 minutes)

## Description

As users and organizations we are more and more relying on technology for everything we do, weather it is our personal usage or our business usage.

How much do we need to trust our technology providers to handle our data with care? Is there any way we can be more in control of our data?

The CryptPad project is being build based on a new approach, end-to-end encryption, raising the bar of data security and privacy, while providing a high usability and many collaboration features, including real-time collaboration.

In this talk, I will present the challenges we are tackling with CryptPad, what specific open source technology we have used and developed for CryptPad and how far we have advanced to provide an end-to-end encrypted collaboration suite.

## Target audience

-   Any users of current cloud products which don't protect their data properly.
-   Developers that are interested in the technological approach used by CryptPad to provide an end-to-end encrypted collaboration tool.

## Speaker(s)

**Ludovic Dubost**: XWiki SAS, XWiki, CryptPad.

Creator of XWiki and CEO of XWiki SAS, Ludovic has been the gentle organizer of the XWiki SAS company for 15 years.

XWiki SAS, only building free & open-source software leads the development of the XWiki Software used by thousands of organizations and helps companies and organizations all over the world organize, share, and collaborate on content. XWiki also leads the development of CryptPad (<https://cryptpad.fr>), the first Zero Knowledge Realtime Collaborative Editor and Drive.

A graduate of Ecole Polytechnique (X90) and Telecom Paris (95), Ludovic Dubost started his career as a software architect for Netscape Communications Europe. He then joined NetValue as CTO, a company doing online usage analysis. He left NetValue after the company was purchased by Nielsen/NetRatings, before creating and launching XWiki in 2004. Ludovic has been a speaker at various events including Paris Open Source Summit, FOSDEM, OW2 Conference, RMLL, Capitole du Libre, speaking about collaboration software, financing FLOSS software and privacy solutions.

I'm also member of the OpenFoodFacts board.

### Contact(s)

-   **Ludovic Dubost**: @ldubost

## Observations

I've proposed a similar talk at OpenExpo, this talk could be a continuation of that talk, more on the technological side.

## Conditions

-   [x] &nbsp;I agree to follow the [code of conduct](https://eslib.re/conducta/) and request this acceptance from the attendees and speakers.
-   [x] &nbsp;At least one person among those proposing will be present on the day scheduled for the talk.
