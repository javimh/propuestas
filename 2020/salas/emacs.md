---
layout: 2020/post
section: propuestas
category: devrooms
title: Emacs
state: confirmed
---

Desde la comunidad Emacs (tu editor hackeable) queremos proponer un espacio de encuentro para compartir experiencias en forma de charlas/talleres, en el marco de la conferencia esLibre en su edición 2020.

Estamos organizando las charlas/talleres a celebrar en la sala Emacs [en este pad](https://pad.ingobernable.net/p/es-libre).

## Comunidad o grupo que lo propone

**Emacs Org-mode** somos un grupo de apasionad@s de Emacs y Org-mode que viene conversando, [escribiendo blogposts](http://planet.emacs-es.org/) , reuniéndose para realizar charlas/talleres. No encontramos para chatear en el [grupo de Telegram](https://t.me/emacsorgmode).

### Contactos

-   **Adolfo Antón**: adolflow at gmail dot com
-   **David Arroyo**: davidam at gnu dot org
-   **Jordi López**: jordila at librebits dot info

## Público objetivo

Si nos paramos a pensar un momento nos damos cuenta de que un editor de texto nos acompaña durante en gran parte de las tareas que realizamos a través de nuestros teclados.

La sala Emacs quiere abrirse a la participación de todo aquel/la interesad@ en aprender y mejorar su destreza en el manejo del editor extensible y personalizable.

## Tiempo

Un día.

## Día

Segundo día (sábado 6 de junio).

## Formato

-   Talleres (~25')
-   Charlas relámpago (~10')

Por ahora, sujetas a cambios, queremos ofrecer propuestas como:

-   Charla: «La vida de un psicólogo en texto plano» (Notxor)
-   Taller: «introducción a Elisp para no programadores» (Notxor)
-   Taller: «Publicación web con Org-page» (Notxor)
-   «Hacer de Emacs un IDE completo para programar» (JPA)
-   Taller: «Introducción a Emacs y Org-mode» (Flow)
-   Taller: «Programación literaria con Org-mode (Flow)
-   Taller: «Tablas y cálculos en Org-mode (Flow)
-   Taller: «Presentaciones con reveal.js vía ox-reveal» (Flow)
-   Taller: «Publicación web con Hugo y ox-hugo» (Flow)
-   «Intro a la programación Elisp de Org-mode (davidam)
-   «Investigación reproducible con Emacs y R» (davidam)
-   Charla relámpago: «44 años después: de Emacs a la pandemia systemd» (brucelee)

## Comentarios

Ninguno.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que proponen la devroom estará presente el día agendado para la _devroom_.
-   [x] &nbsp;Acepto coordinarme con la organización de esLibre.
-   [x] &nbsp;Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la _devroom_ podría retirarse.
