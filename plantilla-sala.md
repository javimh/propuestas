---
layout: 2020/post
section: propuestas
category: devrooms
title: [NOMBRE DE LA SALA]
---

Pequeña introducción y motivación de la misma. Ten en cuenta que la idea de la sala es realizar una actividad semiautónoma dentro de esLibre, que tendrá normalmente su propia petición de contribuciones, sus propios organizadores, su propio horario (coordinado con el de esLibre), etc.

## Comunidad o grupo que lo propone

Descripción del grupo que lo ha propuesto, con enlace a su web y otras actividades organizadas. Identificar también a las personas que van a estar encargadas de la misma.

La temática tiene que estar en todo caso centrada en el software, cultura y hardware libre y datos abiertos.

### Contactos

* Nombre: contacto

Para "Nombre", utliza el nombre completo. Para "contacto", utiliza una dirección de correo (formato "usuario @ dominio"), o el nombre de usuario en GitLab (formato "usuario @ GitLab"). En cualquier caso, ten en cuenta que estas direcciones se usarán para entrar en contacto contigo, así que mejor si las consultas frecuentemente ;-)

## Público objetivo

¿A quién va dirigida?

## Tiempo

Decir si se va a hacer por un día o por medio día.

## Día

Indicar si se prefiere agendar la *devroom* el primer día o el segundo.

## Formato

Formato de la devroom y cómo se va a hacer la petición de contribuciones y la llamada a la participación. El formato se deja totalmente abierto a los que la propongan, y puede incluir (pero no sólo)

* Talleres
* Charlas relámpago
* Charlas plenarias
* Mesas redondas
* Un hackatón que abarque todo el día

Para que todo el mundo pueda organizar sus viajes, es necesario que el programa de la sala esté completo al menos un mes antes del Congreso, con los ponentes que puedan participar ya confirmados.

## Comentarios

Cualquier otro comentario relevante.

## Condiciones

* [ ] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [ ] &nbsp;Al menos una persona entre los que proponen la devroom estará presente el día agendado para la *devroom*.
* [ ] &nbsp;Acepto coordinarme con la organización de esLibre.
* [ ] &nbsp;Entiendo que si no hay un programa terminado para la fecha que establezca la organización, la *devroom* podría retirarse.
